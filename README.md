# Bleeding edge version
## Video Player and Android Auto Installer for MZD Connect (MAZDA Infotainment)
Install script for Video Player 2([vic_bam85](http://mazda3revolution.com/forums/1940186-post6015.html)) and [Unofficial Android Auto](https://github.com/gartnera/headunit) (mod installer 1.0 by Khantaena)

## Working for
* 56.00.100A-ADR
* 56.00.230A-ADR
* 56.00.240B-ADR
* 56.00.513C-ADR

## CHANGELOG
* Android Auto https://github.com/gartnera/headunit/commit/d0706eab21dbac9fca2ca3c967ded59e9faf1da6
* Video Player v2.7
 - Pause/Fast Forward/Rewind supported
* Reorganize Installer/Uninstall scripts

## Showcase
<a href="http://www.youtube.com/watch?feature=player_embedded&v=-bDfWcSwieY" target="_blank"><img src="http://img.youtube.com/vi/-bDfWcSwieY/0.jpg" alt="AR4 0.9 : Showcase MAZDA Tweaks (Community Version)" width="240" height="180" border="10" /></a>

## Source

- https://github.com/gartnera/headunit/
- http://mazda3revolution.com/forums/1937042-post6007.html
