#!/bin/sh

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /
/jci/bin/jci-log.sh stop

MYDIR=$(dirname "$(readlink -f "$0")")
. ${MYDIR}/utils.sh

CMU_SW_VER=$(get_cmu_sw_version)
rm -f "${MYDIR}/AIO_log.txt"

/jci/tools/jci-dialog --3-button-dialog --title="Tweaks Selection for AR4" --text="Choosing Method" --ok-label="Install" --cancel-label="Uninstall" --button3-label="Skip"
choice=$?

if [ "$choice" = '0' ]; then
 msg="Install.."
 check_compat
 show_message "INSTALLATION STARTED (UNINSTALL FIRST)"
 sh ${MYDIR}/uninstall_AR4.sh
 sh ${MYDIR}/install_AR4.sh
elif [ "$choice" = '1' ]; then
 msg="Uninstall.."
 sh ${MYDIR}/uninstall_AR4.sh
 killall jci-dialog
 reboot
else
 msg="Abort.."
 show_message "INSTALLATION ABORTED PLEASE UNPLUG USB DRIVE"
fi

