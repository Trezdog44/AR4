#!/bin/sh

MYDIR=$(dirname "$(readlink -f "$0")")
source ${MYDIR}/utils.sh
CMU_SW_VER=$(get_cmu_sw_version)

log_message "=== START LOGGING INSTALLING... ==="
log_message "=== AR4 Based on AIO 1.51  -  2017.04.29 (Rollback AA)==="
log_message "=== CMU_SW_VER = ${CMU_SW_VER} ==="
log_message "=== MYDIR = ${MYDIR} ==="

# first test, if copy from MZD to sd card is working to test correct mount point
cp /jci/sm/sm.conf "${MYDIR}"
if [ -e "${MYDIR}/sm.conf" ]
  then
    log_message "=== Copytest to sd card successful, mount point is OK ==="
    log_message " "
    rm -f "${MYDIR}/sm.conf"
  else
    log_message "=== Copytest to sd card not successful, mount point not found! ==="
    /jci/tools/jci-dialog --title="ERROR!" --text="Mount point not found, have to reboot again" --ok-label='OK' --no-cancel &
    sleep 5
    reboot
    exit
fi

# a window will appear for 4 seconds to show the beginning of installation
show_message "START OF TWEAK INSTALLATION\n\n(This and the following message popup windows\n DO NOT have to be confirmed with OK)"
log_message " "

# disable watchdogs in /jci/sm/sm.conf to avoid boot loops if somthing goes wrong
if [ ! -e /jci/sm/sm.conf.org ]
  then
    cp -a /jci/sm/sm.conf /jci/sm/sm.conf.org
    log_message "=== Backup of /jci/sm/sm.conf to sm.conf.org ==="
  else log_message "=== Backup of /jci/sm.conf.org already there! ==="
fi
sed -i 's/watchdog_enable="true"/watchdog_enable="false"/g' /jci/sm/sm.conf
sed -i 's|args="-u /jci/gui/index.html"|args="-u /jci/gui/index.html --noWatchdogs"|g' /jci/sm/sm.conf
log_message "=== WATCHDOG IN SM.CONF PERMANENTLY DISABLED ==="

# -- Enable userjs and allow file XMLHttpRequest in /jci/opera/opera_home/opera.ini - backup first - then edit
if [ ! -e /jci/opera/opera_home/opera.ini.org ]
  then
    cp -a /jci/opera/opera_home/opera.ini /jci/opera/opera_home/opera.ini.org
    log_message "=== Backup of /jci/opera/opera_home/opera.ini to opera.ini.org ==="
  else log_message "=== Backup of /jci/opera/opera_home/opera.ini.org already there! ==="
fi
sed -i 's/User JavaScript=0/User JavaScript=1/g' /jci/opera/opera_home/opera.ini
count=$(grep -c "Allow File XMLHttpRequest=" /jci/opera/opera_home/opera.ini)
if [ "$count" = "0" ]
  then
    sed -i '/User JavaScript=.*/a Allow File XMLHttpRequest=1' /jci/opera/opera_home/opera.ini
  else
    sed -i 's/Allow File XMLHttpRequest=.*/Allow File XMLHttpRequest=1/g' /jci/opera/opera_home/opera.ini
fi
log_message "=== ENABLED USERJS AND ALLOWED FILE XMLHTTPREQUEST IN /JCI/OPERA/OPERA_HOME/OPERA.INI  ==="
log_message " "

# Remove fps.js if still exists
if [ -e /jci/opera/opera_dir/userjs/fps.js ]
  then mv /jci/opera/opera_dir/userjs/fps.js /jci/opera/opera_dir/userjs/fps.js.org
  log_message "=== Moved /jci/opera/opera_dir/userjs/fps.js to fps.js.org  ==="
  log_message " "
fi

# Enable WIFI
if grep -Fq "(framework.localize.REGIONS['NorthAmerica'])" /jci/gui/apps/syssettings/js/syssettingsApp.js
  then
    cp /jci/gui/apps/syssettings/js/syssettingsApp.js "${MYDIR}/syssettingsApp_WIFI-before.js"
    LN=$(grep -n "if((region != (framework.localize.REGIONS\['NorthAmerica'\]) [&|]\{2\} (region != (framework.localize.REGIONS\['Japan'\]))))" /jci/gui/apps/syssettings/js/syssettingsApp.js | cut -d":" -f1)
    if [ ! -z "$LN" ]; then
      sed -i "$LN"'s/NorthAmerica/Japan/' /jci/gui/apps/syssettings/js/syssettingsApp.js
      log_message "=== Wifi for NA enabled ==="
    else
      log_message "=== ERROR: Line For Wifi Fix Not Found ==="
    fi
    cp /jci/gui/apps/syssettings/js/syssettingsApp.js "${MYDIR}/syssettingsApp_WIFI-after.js"
  else
    echo "exist"
    log_message "=== Wifi already active ==="
    log_message " "
fi

# disable/enable touchscreen mod: enable = mod off, disable = mod on
show_message "ENABLE TOUCHSCREEN ..."
/jci/scripts/set_lvds_speed_restriction_config.sh disable
/jci/scripts/set_speed_restriction_config.sh disable
log_message "=== TOUCHSCREEN IS ENABLED ==="
log_message " "

# Install Android Auto Headunit App
show_message "INSTALL ANDROID AUTO HEADUNIT APP ..."
log_message "BEGIN INSTALLATION OF ANDROID AUTO HEADUNIT APP ==="

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_androidauto-before.sh
cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_androidauto-1._before.json
cp /jci/sm/sm.conf ${MYDIR}/sm_androidauto-before.conf

cp -a ${MYDIR}/config/androidauto/data_persist/dev/* /tmp/mnt/data_persist/dev
cp -a ${MYDIR}/config/androidauto/jci/gui/apps/_androidauto /jci/gui/apps
cp -a ${MYDIR}/config/androidauto/usr/lib/gstreamer-0.10/libgsth264parse.so /usr/lib/gstreamer-0.10
chmod 755 /usr/lib/gstreamer-0.10/libgsth264parse.so
chmod 755 /tmp/mnt/data_persist/dev/bin/websocketd
chmod 755 /tmp/mnt/data_persist/dev/bin/headunit
chmod 755 /tmp/mnt/data_persist/dev/bin/headunit-wrapper

if [ ! -e /usr/lib/gstreamer-0.10/libgstalsa.so.org ]
  then
    cp -a ${MYDIR}/config/androidauto/usr/lib/gstreamer-0.10/libgstalsa.so /usr/lib/gstreamer-0.10/libgstalsa.so.org
    log_message "=== Backup of /usr/lib/gstreamer-0.10/libgstalsa.so to libgstalsa.so.org ==="
fi
cp -a ${MYDIR}/config/androidauto/usr/lib/gstreamer-0.10/libgstalsa.so /usr/lib/gstreamer-0.10
sleep 2
chmod 755 /usr/lib/gstreamer-0.10/libgstalsa.so

log_message "=== Clean obsolete input_filter to /jci/sm/sm.conf ==="
if grep -Fq "input_filter" /jci/sm/sm.conf
  then
    sed -i '/input_filter/ d' /jci/sm/sm.conf
fi

log_message "=== Copied Android Auto Headunit App files ==="

# delete empty lines
sed -i '/^$/ d' /jci/scripts/stage_wifi.sh
sed -i '/#!/ a' /jci/scripts/stage_wifi.sh

# check for 1st line of stage_wifi.sh
if grep -Fq "#!/bin/sh" /jci/scripts/stage_wifi.sh
  then
    echo "OK"
    log_message "=== 1st line of stage_wifi.sh is OK ==="
  else
    cp -a ${MYDIR}/config/androidauto/stage_wifi.sh /jci/scripts/
    log_message "=== Missing 1st line of stage_wifi.sh, copied new one ==="
fi

# add commands for Android Auto to stage_wifi
if [ -e /jci/scripts/stage_wifi.sh ]
  then
    if grep -Fq "# Android Auto start" /jci/scripts/stage_wifi.sh
      then
        echo "exist"
        log_message "=== Android Auto entry already exists in /jci/scripts/stage_wifi.sh ==="
      else
        #first backup
        cp -a /jci/scripts/stage_wifi.sh /jci/scripts/stage_wifi.sh.AA
        log_message "=== Backup of /jci/scripts/stage_wifi.sh to stage_wifi.sh.AA ==="
        echo -e "\n\n### Android Auto start" >> /jci/scripts/stage_wifi.sh
        sleep 1
        echo -e "websocketd --port=9999 sh &" >> /jci/scripts/stage_wifi.sh
        log_message "=== Added Android Auto entry to /jci/scripts/stage_wifi.sh ==="
        cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_androidauto-after.sh
    fi
fi

# copy additionalApps.js, if not already present
if [ ! -e /jci/opera/opera_dir/userjs/additionalApps.js ]
then
  log_message "=== No additionalApps.js available, will copy one ==="
  cp -a ${MYDIR}/config/androidauto/jci/opera/opera_dir/userjs/additionalApps.* /jci/opera/opera_dir/userjs/
  find /jci/opera/opera_dir/userjs/ -type f -name '*.js' -exec chmod 755 {} \;
fi

# create new additionalApps.json file from scratch if not already present
if [ ! -e /jci/opera/opera_dir/userjs/additionalApps.json ]
then
  log_message "=== No additionalApps.json available, generating one ==="
  echo "[" > /jci/opera/opera_dir/userjs/additionalApps.json
  echo "]" >> /jci/opera/opera_dir/userjs/additionalApps.json
  cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_generated.json
  chmod 755 /jci/opera/opera_dir/userjs/additionalApps.json
fi

# call function add_app_json to modify additionalApps.json
add_app_json "_androidauto" "Android Auto"

log_message "END INSTALLATION OF ANDROID AUTO HEADUNIT APP"
log_message " "


# Videoplayer v2 by Waisky2
show_message "VIDEOPLAYER V2 BY WAISKY2\n MODS BY VIC_BAM85"
log_message "BEGIN INSTALLATION OF VIDEOPLAYER V2"

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_videoplayer-before.sh
cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_videoplayer-1._before.json

### Copy tweak files to MZD
cp -a ${MYDIR}/config/videoplayer/jci/gui/apps/* /jci/gui/apps/
log_message "=== Copied folder /jci/gui/apps/_videoplayer ==="
find /jci/gui/apps/_*/ -type f -name '*.js' -exec chmod 755 {} \;
find /jci/gui/apps/_*/ -type f -name '*.sh' -exec chmod 755 {} \;

if [ ! -e /jci/gui/addon-common/websocketd ]  || [ ! -e /jci/gui/addon-common/jquery.min.js ]; then
    cp -a ${MYDIR}/config/videoplayer/jci/gui/addon-common/ /jci/gui/
  chmod 755 /jci/gui/addon-common/websocketd
  log_message "=== Copied websocketd and jquery.min.js to /jci/gui/addon-common/ ==="
  else
    log_message "=== websocketd and jquery.min.js available, no copy needed ==="
fi

if [ ! -e /bin/busybox-armv7l ]; then
  cp -a ${MYDIR}/config/bin/busybox-armv7l /bin/
  log_message "=== Copied /bin/busybox-armv7l ==="
  chmod 755 /bin/busybox-armv7l
  if [ -e /usr/bin/nc ]; then
   mv /usr/bin/nc /usr/bin/nc.org
  fi
  ln -s /bin/busybox-armv7l /usr/bin/nc
  log_message "=== Modified file rights of new busybox and make new link to /usr/bin/nc ==="
fi

# check for 1st line of stage_wifi.sh
if grep -Fq "#!/bin/sh" /jci/scripts/stage_wifi.sh
  then
    echo "OK"
    log_message "=== 1st line of stage_wifi.sh is OK ==="
  else
    cp -a ${MYDIR}/config/videoplayer/stage_wifi.sh /jci/scripts/
    log_message "=== Missing 1st line of stage_wifi.sh, copied new one ==="
fi

# add commands for videoplayer to stage_wifi.sh
if [ -e /jci/scripts/stage_wifi.sh ]
  then
    if grep -Fq "### Video player" /jci/scripts/stage_wifi.sh
      then
        echo "exist"
        log_message "=== Videoplayer entry already exists in /jci/scripts/stage_wifi.sh ==="
      else
        sed -i '/#!/ a\### Video player' /jci/scripts/stage_wifi.sh
        sleep 1
        sed -i '/Video player/ i\' /jci/scripts/stage_wifi.sh
    fi
    if grep -Fq "websocketd --port=9998 sh" /jci/scripts/stage_wifi.sh
      then
        echo "exist"
      else
        sed -i '/### Video player/ a\/jci/gui/addon-common/websocketd --port=9998 sh &' /jci/scripts/stage_wifi.sh
        log_message "=== Added videoplayer entry to /jci/scripts/stage_wifi.sh ==="
        cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_videoplayer-after.sh
    fi
fi

# copy additionalApps.js, if not already present
if [ ! -e /jci/opera/opera_dir/userjs/additionalApps.js ]
then
  log_message "=== No additionalApps.js available, will copy one ==="
  cp -a ${MYDIR}/config/videoplayer/jci/opera/opera_dir/userjs/additionalApps.* /jci/opera/opera_dir/userjs/
  find /jci/opera/opera_dir/userjs/ -type f -name '*.js' -exec chmod 755 {} \;
fi

# create new additionalApps.json file from scratch if not already present
if [ ! -e /jci/opera/opera_dir/userjs/additionalApps.json ]
then
  log_message "=== No additionalApps.json available, generating one ==="
  echo "[" > /jci/opera/opera_dir/userjs/additionalApps.json
  echo "]" >> /jci/opera/opera_dir/userjs/additionalApps.json
  cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_generated.json
  chmod 755 /jci/opera/opera_dir/userjs/additionalApps.json
fi

# call function add_app_json to modify additionalApps.json
add_app_json "_videoplayer" "Video Player"

log_message "END INSTALLATION OF VIDEOPLAYER"
log_message " "
log_message " "
sleep 2
log_message "END OF TWEAKS INSTALLATION"
sleep 3
killall jci-dialog
sleep 3

# a window will appear for asking to reboot automatically
/jci/tools/jci-dialog --confirm --title="SELECTED AIO TWEAKS APPLIED" --text="Click OK to reboot the system"
    if [ $? != 1 ]
    then
      sleep 10
      reboot
      exit
    fi
sleep 2
killall jci-dialog
