#!/bin/sh

MYDIR=$(dirname "$(readlink -f "$0")")
source ${MYDIR}/utils.sh
CMU_SW_VER=$(get_cmu_sw_version)

# a window will appear for 4 seconds to show the beginning of installation
show_message "START OF TWEAK DEINSTALLATION\n\n(This and the following message popup windows\n DO NOT have to be confirmed with OK)"

# disable/enable touchscreen mod: enable = mod off, disable = mod on
show_message "DISABLE TOUCHSCREEN ..."
/jci/scripts/set_lvds_speed_restriction_config.sh enable
/jci/scripts/set_speed_restriction_config.sh enable
log_message "=== TOUCHSCREEN IS DISABLED ==="
log_message " "

# Remove Android Auto Headunit App
show_message "REMOVE ANDROID AUTO HEADUNIT APP ..."
log_message "REMOVE ANDROID AUTO HEADUNIT APP ..."

### kills all WebSocket daemons
pkill websocketd

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_androidauto-before.sh
cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_androidauto-1._before.json
cp /jci/sm/sm.conf ${MYDIR}/sm_androidauto-before.conf

rm -fr /tmp/mnt/data_persist/dev/androidauto
rm -f /tmp/mnt/data_persist/dev/bin/websocketd
rm -f /tmp/mnt/data_persist/dev/bin/headunit
rm -f /tmp/mnt/data_persist/dev/bin/aaserver
rm -f /tmp/mnt/data_persist/dev/bin/headunit-wrapper
rm -f /tmp/mnt/data_persist/dev/bin/headunit/input_filter
rm -rf /tmp/mnt/data_persist/dev/bin/headunit_libs
rm -fr /jci/gui/apps/_androidauto
rm -f /tmp/mnt/data/enable_input_filter
rm -f /tmp/mnt/data/input_filter
rm -f /usr/lib/gstreamer-0.10/libgsth264parse.so
log_message "=== Removed files for Android Auto Headunit App ==="

if test -s /usr/lib/gstreamer-0.10/libgstalsa.so.org
  then
    rm -f /usr/lib/gstreamer-0.10/libgstalsa.so
    log_message "=== Original libgstalsa.so is available as backup ==="
    mv /usr/lib/gstreamer-0.10/libgstalsa.so.org /usr/lib/gstreamer-0.10/libgstalsa.so
    /bin/fsync /usr/lib/gstreamer-0.10/libgstalsa.so
  else
    cp -a ${MYDIR}/config_org/androidauto/usr/lib/gstreamer-0.10/libgstalsa.so /usr/lib/gstreamer-0.10/
    chmod 755 /usr/lib/gstreamer-0.10/libgstalsa.so
fi

# delete Android Auto entry from /jci/opera/opera_dir/userjs/additionalApps.json
if grep -Fq "_speedometer" /jci/opera/opera_dir/userjs/additionalApps.json
  then
    log_message "=== Found speedometer entry in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_speedometer=1
  else
    log_message "=== No speedometer entry found in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_speedometer=0
fi
if grep -Fq "_videoplayer" /jci/opera/opera_dir/userjs/additionalApps.json
  then
    log_message "=== Found videoplayer entry in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_videoplayer=1
  else
    log_message "=== No videoplayer entry found in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_videoplayer=0
fi
if [ ${appmenu_speedometer} = "0" ] && [ ${appmenu_videoplayer} = "0" ]
  then
    log_message "=== No more entrys in additionalApps.json, files will be deleted ==="
    rm -f /jci/opera/opera_dir/userjs/additionalApps.*
  else
    remove_app_json "_androidauto"
fi

sed -i '/Android Auto start/d' /jci/scripts/stage_wifi.sh
sed -i '/hwclock --hctosys/d' /jci/scripts/stage_wifi.sh
sed -i '/headunit-wrapper/d' /jci/scripts/stage_wifi.sh
sed -i '/websocketd --port=9999/d' /jci/scripts/stage_wifi.sh
sed -i '/websocketd --port=9998/d' /jci/scripts/stage_wifi.sh
sed -i '/websocketd --port=9997/d' /jci/scripts/stage_wifi.sh
log_message "=== Deleted modifications from /jci/scripts/stage_wifi.sh ==="

rm -f /jci/scripts/stage_wifi.sh.org3
rm -f /jci/scripts/stage_wifi.sh.AA

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_androidauto-after.sh
cp /jci/sm/sm.conf ${MYDIR}/sm_androidauto-after.conf

if [ -e /usr/bin/nc.org ]; then
  rm -f /usr/bin/nc
  mv /usr/bin/nc.org /usr/bin/nc
fi

log_message "END DEINSTALLATION OF ANDROID AUTO HEADUNIT APP"
log_message " "


# Remove videoplayer v2
show_message "REMOVE VIDEOPLAYER V2 ..."
log_message "REMOVE VIDEOPLAYER V2 ..."

### kills all WebSocket daemons
pkill websocketd

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_videoplayer-before.sh
cp /jci/opera/opera_dir/userjs/additionalApps.json ${MYDIR}/additionalApps_videoplayer-1._before.json

sed -i '/Speedo-Compass-Video_Tweak/d' /jci/scripts/stage_wifi.sh
sed -i '/v3.2/d' /jci/scripts/stage_wifi.sh
sed -i '/Removed requirement/d' /jci/scripts/stage_wifi.sh
sed -i '/# mount /d' /jci/scripts/stage_wifi.sh
sed -i '/Added additional/d' /jci/scripts/stage_wifi.sh
sed -i '/get-vehicle-speed.sh/d' /jci/scripts/stage_wifi.sh
sed -i '/get-vehicle-data-other.sh/d' /jci/scripts/stage_wifi.sh
sed -i '/get-gps-data.sh/d' /jci/scripts/stage_wifi.sh
sed -i '/Need to set defaults/d' /jci/scripts/stage_wifi.sh
sed -i '/myVideoList /d' /jci/scripts/stage_wifi.sh
sed -i '/playbackAction /d' /jci/scripts/stage_wifi.sh
sed -i '/playbackOption /d' /jci/scripts/stage_wifi.sh
sed -i '/playbackStatus /d' /jci/scripts/stage_wifi.sh
sed -i '/playback/d' /jci/scripts/stage_wifi.sh
sed -i '/myVideoList/d' /jci/scripts/stage_wifi.sh
sed -i '/Video player action watch/d' /jci/scripts/stage_wifi.sh
sed -i '/playback-action.sh/d' /jci/scripts/stage_wifi.sh
sed -i '/Log data collection/d' /jci/scripts/stage_wifi.sh
sed -i '/get-log-data/d' /jci/scripts/stage_wifi.sh
sed -i '/### Video player/d' /jci/scripts/stage_wifi.sh
sed -i '/_videoplayer/d' /jci/scripts/stage_wifi.sh
sed -i '/55555/d' /jci/scripts/stage_wifi.sh
sed -i '/addon-player.sh &/d' /jci/scripts/stage_wifi.sh

# delete videoplayer entry from /jci/opera/opera_dir/userjs/additionalApps.json
if grep -Fq "_androidauto" /jci/opera/opera_dir/userjs/additionalApps.json
  then
    log_message "=== Found androidauto entry in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_androidauto=1
  else
    log_message "=== No androidauto entry found in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_androidauto=0
fi
if grep -Fq "_speedometer" /jci/opera/opera_dir/userjs/additionalApps.json
  then
    log_message "=== Found speedometer entry in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_speedometer=1
  else
    log_message "=== No speedometer entry found in /jci/opera/opera_dir/userjs/additionalApps.json ==="
    appmenu_speedometer=0
fi
if [ ${appmenu_androidauto} = "0" ] && [ ${appmenu_speedometer} = "0" ]
  then
    log_message "=== No more entrys in additionalApps.json, files will be deleted ==="
    rm -f /jci/opera/opera_dir/userjs/additionalApps.*
  else
    remove_app_json "_videoplayer"
fi

### cleanup old versions
if [ ! -d /jci/gui/apps/_speedometer ]; then
  rm -fr /jci/gui/addon-common
  log_message "=== Removed /jci/gui/addon-common because speedometer isn't installed neither ==="
fi
rm -fr /jci/gui/addon-player
rm -fr /jci/gui/addon-speedometer
rm -fr /jci/gui/speedometer
rm -fr /jci/gui/apps/_videoplayer
rm -f /jci/opera/opera_dir/userjs/addon-startup.js
rm -f /jci/opera/opera_dir/userjs/mySpeedometer*
mv /jci/opera/opera_dir/userjs/fps.js /jci/opera/opera_dir/userjs/fps.js.org
rm -f /jci/scripts/get-gps-data*
rm -f /jci/scripts/get-log-data*
rm -f /jci/scripts/get-vehicle-data-other*
rm -f /jci/scripts/get-vehicle-gear*
rm -f /jci/scripts/get-vehicle-speed*
rm -f /jci/scripts/stage_wifi.sh.bak?

cp /jci/scripts/stage_wifi.sh ${MYDIR}/stage_wifi_videoplayer-after.sh

log_message "REMOVED VIDEOPLAYER V2"
log_message " "
log_message "END OF TWEAKS DEINSTALLATION"
