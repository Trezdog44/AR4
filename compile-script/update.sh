#!/bin/bash

user="Yourname"
email="YourEmail"

REPO="gartnera-master izacus-master mishaaq-backup_camera_fix"
DATE=`date +%Y-%m-%d:%H:%M:%S`
DIR="/home/user/repo"

cd $DIR

for repo in $REPO 
do
 USER=`echo $repo|awk -F'-' '{print $1}'`
 cd $DIR/$repo
 git pull
 SHA1=`git rev-parse HEAD`
 LOG=`git log --pretty=oneline --abbrev-commit |head -1`
 cd mazda
 cp Makefile Makefile.org
 patch -p0 -b < ${DIR}/mzdonline/p1.patch
 patch -p0 -b < ${DIR}/mzdonline/p2.patch
 sed -i 's/-MD -g/-Os -s/g' Makefile
 make clean
 make
 rm -f Makefile
 mv Makefile.orig Makefile
 mv main.cpp.orig main.cpp
 if [ -e headunit ]
 then
  cd $DIR/mzdonline/AR4-${repo}
  git pull
  yes | cp -rf $DIR/$repo/mazda/headunit $DIR/mzdonline/AR4-${repo}/installer/config/androidauto/data_persist/dev/bin/headunit
  git config --global user.email "$email"
  git config --global user.name "$user"
  git config --global push.default matching
  git commit installer/config/androidauto/data_persist/dev/bin/headunit -m "Automate Daily Build : $DATE
   - https://github.com/$USER/headunit/commit/$SHA1 $LOG"
  git config --global push.default simple
  git push
  cd ..
 fi
done
